# Test objects

Some very small objects that are used to for testing.

The objects should NOT be used for anything besides testing that fetching actually works. No objects should be used after that point, but one should rather create the objects inside the test.